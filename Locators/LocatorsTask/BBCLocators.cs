using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using LocatorsTask.ExternalHelpClasses;
using static System.Net.WebRequestMethods;

namespace LocatorsTask
{
    public class Tests
    {
        readonly string testUrl = "https://www.bbc.com/sport";
        IWebDriver driver;

        [OneTimeSetUp]
        public void Setup()
        {
            driver = new ChromeDriver(@"D:\Studing\SELF-EDUCATION\AQA EPAM\C#\Test Automation\locators\Locators\LocatorsTask\driver");
            driver.Url = "https://www.bbc.com/sport";
            driver.Manage().Window.Maximize();

        }

        [TestCase("BBCLogo")]
        [TestCase("SignInLogo")]
        [TestCase("HomeBTN")]
        [TestCase("SportBTN")]
        [TestCase("ReelBTN")]
        [TestCase("WorklifeBTN")]
        [TestCase("TravelBTN")]
        public void HeaderItemsDisplayed(string key)
        {
            
            string cssSelector = TestData.HeaderKeys[key];
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            
            if (key == "BBCLogo")
            {
                driver.Url = testUrl;
            }
            IWebElement presentedItem = driver.FindElement(By.CssSelector(cssSelector));
            Assert.IsTrue(presentedItem.Displayed);
        }


        [TestCase("Home")]
        [TestCase("Football")]
        [TestCase("Cricket")]
        [TestCase("Formula 1")]
        [TestCase("Rugby U")]
        [TestCase("Tennis")]
        [TestCase("Golf")]
        [TestCase("Athletics")]
        [TestCase("Cycling")]
        public void SportSections(string value)
        {
            driver.Url = "https://www.bbc.com/sport";
            driver.Manage().Window.Maximize();
            IWebElement presentedItem = driver.FindElement(By.LinkText(value));
            Assert.IsTrue(presentedItem.Displayed);
        }

        [TestCase("Youtube")]
        [TestCase("Instagram")]
        [TestCase("Facebook")]
        [TestCase("Twitter")]
        public void SocialNetworks(string key)
        {
            driver.Url = "https://www.bbc.com/sport";
            string linkMainPage = TestData.NetworksMainLinks[key];
            IWebElement linkToSocial = driver.FindElement(By.XPath(linkMainPage));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            linkToSocial.Click();
            if (key == "Youtube")
            {
                IWebElement acceptAllBtn = driver.FindElement(By.CssSelector("span.VfPpkd-vQzf8d[aria-hidden=\"true\"][jsname=\"V67aGc\"]"));
                acceptAllBtn.Click();
            }

            if (key == "Facebook")
            {
                IWebElement allowAll =
                    driver.FindElement(By.CssSelector("body > div.__fb-light-mode.x1n2onr6.x1vjfegm > div.x9f619.x1n2onr6.x1ja2u2z > div > div.x1uvtmcs.x4k7w5x.x1h91t0o.x1beo9mf.xaigb6o.x12ejxvf.x3igimt.xarpa2k.xedcshv.x1lytzrv.x1t2pt76.x7ja8zs.x1n2onr6.x1qrby5j.x1jfb8zj > div > div > div > div.xzg4506.x1l90r2v.x1pi30zi.x1swvt13 > div > div:nth-child(2) > div.x1i10hfl.xjbqb8w.x6umtig.x1b1mbwd.xaqea5y.xav7gou.x1ypdohk.xe8uvvx.xdj266r.x11i5rnm.xat24cr.x1mh8g0r.xexx8yu.x4uap5.x18d9i69.xkhd6sd.x16tdsg8.x1hl2dhg.xggy1nq.x1o1ewxj.x3x9cwd.x1e5q0jg.x13rtm0m.x87ps6o.x1lku1pv.x1a2a7pz.x9f619.x3nfvp2.xdt5ytf.xl56j7k.x1n2onr6.xh8yej3"));
                allowAll.Click();
                IWebElement crossBtn = driver.FindElement(By.CssSelector(
                    "div.x1i10hfl.x6umtig.x1b1mbwd.xaqea5y.xav7gou.x1ypdohk.xe8uvvx.xdj266r.x11i5rnm.xat24cr.x1mh8g0r.x16tdsg8.x1hl2dhg.xggy1nq.x87ps6o.x1lku1pv.x1a2a7pz.x6s0dn4.x14yjl9h.xudhj91.x18nykt9.xww2gxu.x972fbf.xcfux6l.x1qhh985.xm0m39n.x9f619.x78zum5.xl56j7k.xexx8yu.x4uap5.x18d9i69.xkhd6sd.x1n2onr6.xc9qbxq.x14qfxbe.x1qhmfi1"));
                crossBtn.Click();
            }
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            string cssSelector = TestData.SocialNetworksLink[key];
            IWebElement presentedItem = driver.FindElement(By.CssSelector(cssSelector));
            Assert.IsTrue(presentedItem.Displayed);
        }



        [TestCase("Scores & Fixture")]
        [TestCase("Tables")]
        [TestCase("Gossip")]
        [TestCase("Transfers")]
        [TestCase("Top Scores")]
        public void AllSports(string key)
        {

            if (key == "Scores & Fixture")
            {
                IWebElement iframeElement = driver.FindElement(By.Id("sp_message_iframe_783538"));
                driver.SwitchTo().Frame(iframeElement);
                IWebElement buttonInsideIframe = driver.FindElement(By.XPath("//button[@title = \"I agree\"]"));
                buttonInsideIframe.Click();
                driver.SwitchTo().DefaultContent();
                IWebElement cookiesAgree = driver.FindElement(By.CssSelector(
                    ".ssrcss-1fomw6p-ConsentButton.exhqgzu2"));
                cookiesAgree.Click();
                driver.FindElement(By.CssSelector("a.ssrcss-1ec1ytn-StyledLink.eis6szr2")).Click();
            }

            string footbalSectionElement = TestData.FootBallHeader[key];
            IWebElement sectionElment = driver.FindElement(By.XPath(footbalSectionElement));
            Assert.IsTrue(sectionElment.Displayed);
        }


        [OneTimeTearDown]

        public void TearDown()

        {

            driver.Quit();

        }
    }
}