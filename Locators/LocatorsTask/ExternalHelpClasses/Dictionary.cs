﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocatorsTask.ExternalHelpClasses
{
    public class TestData
    {
        public static Dictionary<string, string> HeaderKeys = new Dictionary<string, string>
        {
            { "BBCLogo", "span.ssrcss-fj0f8c-LogoIconWrapper.e1gviwgp11" },
            { "SignInLogo", ".ssrcss-tpup1o-NavigationLink-AccountLink.e1gviwgp14" },
            { "HomeBTN", "li.ssrcss-13wzjg6-GlobalNavigationProduct"},
            { "SportBTN", "span.ssrcss-10wdlej-NavItemHoverState.e1gviwgp19"},
            { "ReelBTN", "span.ssrcss-6mftle-NavItemHoverState.e1gviwgp19"},
            { "WorklifeBTN", "span.ssrcss-1l58p0q-NavItemHoverState.e1gviwgp19"},
            { "TravelBTN", "span.ssrcss-1v60727-NavItemHoverState.e1gviwgp19"},
            { "FutureBTN", "span.ssrcss-qtp2tt-NavItemHoverState.e1gviwgp19"}
        };


        public static Dictionary<string, string> SocialNetworksLink = new Dictionary<string, string>
        {
            { "Youtube", "#logo-icon" },
            {
                "Instagram",
                "._aagx"
            },
            {
                "Facebook",
                "div.x6s0dn4.x78zum5.x1iyjqo2.x1n2onr6\r\n"
            },
            {
                "Twitter",
                "div.css-901oao.r-1awozwy.r-6koalj.r-18u37iz.r-16y2uox.r-37j5jr.r-a023e6.r-b88u0q.r-1777fci.r-rjixqe.r-bcqeeo.r-q4m81j.r-qvutc0"
            }
        };

        public static Dictionary<string, string> NetworksMainLinks = new Dictionary<string, string>
        {
            { "Youtube", "//a[@href='https://www.youtube.com/channel/UCW6-BQWFA70Dyyc7ZpZ9Xlg']" },
            {
                "Instagram",
                "//a[@href='https://www.instagram.com/bbcsport/']"
            },
            {
                "Facebook",
                "//a[@href='https://www.facebook.com/BBCSport']"
            },
            {
                "Twitter",
                "//a[@href='https://twitter.com/BBCSport']"
            }
        };

        public static Dictionary<string, string> FootBallHeader = new Dictionary<string, string>
        {
            { "Scores & Fixture", "//a[normalize-space()='Scores & Fixtures']" },
            { "Tables", "//a[normalize-space()='Tables']" },
            { "Gossip", "//a[normalize-space()='Gossip']" },
            { "Transfers", "//a[normalize-space()='Transfers']" },
            { "Top Scores", "//a[normalize-space()='Top Scorers']" }
        };
    }
}
 